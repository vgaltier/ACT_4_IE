# Mecsyco Tutorial #5

Please read [Tutorial #3](https://gitlab.inria.fr/vgaltier/ACT_4_IE/-/tree/main/MecsycoTutorial/Tutorial_02) first as this example is built on it.

This example illustrates the interaction between 2 models.

The walkers that leave a world are injected into the other world:

![Two interacting random walk models.](images/first_interacting_random_walk.svg)
*Two interacting random walk models.*

In addition, the walker’s color depends on the world where it appears the first time. The walkers from the first world are red and the walkers from the second one are green.


The Mecsyco model is represented as:

![MECSYCO model of two interacting models](images/interacting_models.svg)

*MECSYCO model of two interacting models*


There code is quite similar to Tutorial #3, exception the connection is established between the 2 models instead of back on the same model:

```java
		agent1.addInputCouplingArtifact(couplingArtifact1, "in");
		agent1.addOutputCouplingArtifact(couplingArtifact2, "out");
		agent2.addInputCouplingArtifact(couplingArtifact2, "in");
		agent2.addOutputCouplingArtifact(couplingArtifact1, "out");
```
Result:


![View of two interacting random walk models](images/interacting_models_running.png)

*View of two interacting random walk models*
