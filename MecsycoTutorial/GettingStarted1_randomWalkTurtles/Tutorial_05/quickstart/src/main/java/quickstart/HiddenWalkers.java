package quickstart;

import java.util.ArrayList;

import org.nlogo.agent.Turtle;

import mecsyco.core.type.SimulData;

public class HiddenWalkers implements SimulData {

	private ArrayList<Turtle> walkers;

	public HiddenWalkers() {
		walkers = new ArrayList<Turtle>();
	}

	public void add(Turtle turtle) {
		walkers.add(turtle);
	}

	public ArrayList<Turtle> getWalkers() {
		return walkers;
	}
}
