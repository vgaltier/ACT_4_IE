package quickstart;

import mecsyco.core.agent.EventMAgent;
import mecsyco.core.coupling.CentralizedEventCouplingArtifact;
import mecsyco.core.model.ModelArtifact;

public class Launcher {

	/**
	 * World name (used as frame title)
	 */
	public static final String WORLD_NAME_1 = "Random Turtles World 1";
	public static final String WORLD_NAME_2 = "Random Turtles World 2";
	/**
	 * Frame width
	 */
	public static final int WIDTH = 800;
	/**
	 * Frame height
	 */
	public static final int HEIGHT = 600;
	/**
	 * Turtles color
	 */
	public static final double COLOR_1 = 15; //Red
	public static final double COLOR_2 = 55; //Green
	/**
	 * Turtles size
	 */
	public static final int SIZE = 2;
	/**
	 * Maximum simulation time
	 */
	public static final double MAX_SIMULATION_TIME = 10000;
	/**
	 * Time discretization
	 */
	public static final int TIME_STEP_SIZE = 1;
	/**
	 * Path to the NetLogo model
	 */
	public static final String NETLOGO_MODEL_PATH_1 = "../random_walk_1.nlogo";
	public static final String NETLOGO_MODEL_PATH_2 = "../random_walk_2.nlogo";


	public static void main(String[] args) {
		EventMAgent agent1 = new EventMAgent(WORLD_NAME_1, MAX_SIMULATION_TIME);


		ModelArtifact modelArtifact1 = new RandomWalkModelArtifactWithIO(WORLD_NAME_1, NETLOGO_MODEL_PATH_1,
				TIME_STEP_SIZE,
				COLOR_1, SIZE,
				HEIGHT, WIDTH);

		agent1.setModelArtifact(modelArtifact1);

		EventMAgent agent2 = new EventMAgent(WORLD_NAME_2, MAX_SIMULATION_TIME);


		ModelArtifact modelArtifact2 = new RandomWalkModelArtifactWithIO(WORLD_NAME_2, NETLOGO_MODEL_PATH_2,
				TIME_STEP_SIZE,
				COLOR_2, SIZE,
				HEIGHT, WIDTH);

		agent2.setModelArtifact(modelArtifact2);

		CentralizedEventCouplingArtifact couplingArtifact1 = new CentralizedEventCouplingArtifact();
		CentralizedEventCouplingArtifact couplingArtifact2 = new CentralizedEventCouplingArtifact();

		agent1.addInputCouplingArtifact(couplingArtifact1, "in");
		agent1.addOutputCouplingArtifact(couplingArtifact2, "out");
		agent2.addInputCouplingArtifact(couplingArtifact2, "in");
		agent2.addOutputCouplingArtifact(couplingArtifact1, "out");

		agent1.startModelSoftware();
		agent2.startModelSoftware();
		agent1.setModelParameters();
		agent2.setModelParameters();
		agent1.start();
		agent2.start();
	}
}

