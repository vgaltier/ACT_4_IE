# Mecsyco Tutorial #3

Please read [Tutorial #1](https://gitlab.inria.fr/vgaltier/ACT_4_IE/-/tree/main/MecsycoTutorial/Tutorial_01) first as this example is built on it.

This example illustrates the notions of input and output ports of a model, and how to connect ports using a coupling artifact.

Starting from the previous NetLogo random walkers example, we would like to retrieve the walkers that leave the world and spawn them back at the origin of the world. A simple way to do that is to add an output port to the model that will provide the hidden walkers, and an input port that will inject walkers at the origin of the world, and connect the output port to the input port:

![Random walk model with output and input ports connected with a loopback](images/random_walk_loopback.svg)


## Input and output ports

Input and output ports to a model are added by its model artifact.

In our case, the output port provides the walkers that leave the world, and the walkers received through the input port spawn at the origin of the world.

![Random walk model with output and input ports](images/connectable_random_walk_model.svg)

NetLogo has no notion of port nor event, but using the `command` instruction one can feed data to the model at each step (this provides kind of an input port) and using the `report` instruction one can read information from the model (this constitutes a kind of output port).

We create a `RandomWalkModelArtifactWithIO` that extends `NetLogoModelArtifact` and at each step we "read" the number of hidden walkers and generate an event with their list (the list is implemented as an ArrayList in the `HiddenWalkers` class, that implements `SimulData`), and in the method that processes the event, we "inject" each hidden walker back into the model :

```java
public class RandomWalkModelArtifactWithIO extends NetLogoModelArtifact {

	@Override
	public SimulEvent getExternalOutputEvent(String port) {
		if ("out".equals(port)) {
			AgentSet hiddenTurtles = (AgentSet) getComp().report("turtles with [hidden?]");
			if (hiddenTurtles.count() > 0) {
				Iterator<Agent> turtleIterator = hiddenTurtles.agents().iterator();
				Turtle turtle;
				HiddenWalkers data = new HiddenWalkers();
				while (turtleIterator.hasNext()) {
					turtle = (Turtle) (turtleIterator.next());
					data.add(turtle);
				}
				return new SimulEvent(data, getLastEventTime());
			}
		} 
		return null;
	}

	@Override
	public void processExternalInputEvent(SimulEvent event, String port) {
		if ("in".equals(port)) {
			final SimulData data = event.getData();
			if (data instanceof HiddenWalkers) {
				final HiddenWalkers walkers = (HiddenWalkers) data;
				ArrayList<Turtle> turtles = walkers.getWalkers();
				turtles.forEach(new Consumer<Turtle>() {
					public void accept(Turtle turtle) {
						getComp().command(
								"create-turtles 1 [set size " + turtle.size() + " set color " + turtle.color() + "] ");
					}
				});
			} 
		} 
	}
}
```

## Coupling Artifact

In a MECSYCO model, the connection between output and input ports is modeled with a coupling artifact. It is also a generic component shipped with the core. It links two m-agents (or a m-agent with itself).

![Mecsyco model with a loopback coupling artifact](images/single_random_walk_loopback.svg)

Only three extra lines of code are needed to establish this connection. The 2 last lines specify the destination – the model input - and the source – the model output – of the coupling artifact. In this example, the coupling artifact links the m-agent with itself. Since there is only an input and an output, the chosen port names (“in” and “out”) are not relevant. When there exists several input ports and/or output ports, the port name enables a simple port discrimination in the model artifact.

```java
		CentralizedEventCouplingArtifact couplingArtifact = new CentralizedEventCouplingArtifact();
		agent.addInputCouplingArtifact(couplingArtifact, "in");
		agent.addOutputCouplingArtifact(couplingArtifact, "out");
```

At each tick, the m-agent gets the hidden walkers from the model artifact output and then sends them to the model artifact input. The model artifact creates as many new walkers as received. When you run the MECSYCO model, you note that the count of walkers is constant over time.
