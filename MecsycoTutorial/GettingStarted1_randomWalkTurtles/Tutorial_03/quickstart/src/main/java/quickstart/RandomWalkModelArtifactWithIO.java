package quickstart;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Supplier;

import org.nlogo.agent.AgentSet;
import org.nlogo.agent.Turtle;
import org.nlogo.api.Agent;

import mecsyco.core.exception.UnexpectedTypeException;
import mecsyco.core.type.SimulData;
import mecsyco.core.type.SimulEvent;
import mecsyco.world.netlogo.NetLogoModelArtifact;

/**
 * Tutorial #3 Random walker model 
 * output: walkers that leave the world 
 * input: walkers to be injected at the center of the world
 */
public class RandomWalkModelArtifactWithIO extends NetLogoModelArtifact {

	public RandomWalkModelArtifactWithIO(String modelName, String modelPath, 
			int timeStep, 
			double color, int size, 
			int heigth, int width) {
		super(modelName, modelPath, 
				timeStep, 
				((Supplier<Map<String, String>>) () -> {
					Map<String, String> parameters = new HashMap<String, String>();
					// map of model parameters and corresponding instructions
					parameters.put("color","set g-color " + color); 
					parameters.put("size", "set g-size " + size);
					return parameters;
				}).get(), 
				null, null, 
				((Supplier<Map<String, Object>>) () -> {
					Map<String, Object> initVars = new HashMap<String, Object>();
					// map of model parameters and corresponding instructions
					initVars.put("color", color); 
					initVars.put("size", size);
					return initVars;
				}).get(), 
				heigth, width);
	}


	@Override
	/**
	 * called repeatedly during the simulation (once per time step) get the model
	 * output event (to exchange with another model)
	 * 
	 * output to produce: the list of hidden turtles (with their color-size
	 * attributes)
	 */
	public SimulEvent getExternalOutputEvent(String port) {
		if ("out".equals(port)) {
			// get hidden turtles
			AgentSet hiddenTurtles = (AgentSet) getComp().report("turtles with [hidden?]");

			if (hiddenTurtles.count() > 0) {
				Iterator<Agent> turtleIterator = hiddenTurtles.agents().iterator();
				Turtle turtle;
				HiddenWalkers data = new HiddenWalkers();
				while (turtleIterator.hasNext()) {
					turtle = (Turtle) (turtleIterator.next());
					data.add(turtle);
				}
				return new SimulEvent(data, getLastEventTime());
			}
		} else {
			getLogger().warn("The output port '{}' is not supported", port);
		}
		return null;
	}

	@Override
	/**
	 * called repeatedly during the simulation (when there is an external input
	 * event) process external input event
	 * 
	 * external input event: receives the walkers to create
	 */
	public void processExternalInputEvent(SimulEvent event, String port) {
		if ("in".equals(port)) {
			final SimulData data = event.getData();

			if (data instanceof HiddenWalkers) {
				final HiddenWalkers walkers = (HiddenWalkers) data;
				ArrayList<Turtle> turtles = walkers.getWalkers();
				turtles.forEach(new Consumer<Turtle>() {
					public void accept(Turtle turtle) {
						getComp().command(
								"create-turtles 1 [set size " + turtle.size() + " set color " + turtle.color() + "] ");
					}
				});
			} else {
				getLogger().warn("Input port 'in' ", new UnexpectedTypeException(HiddenWalkers.class, data.getClass()));
			}
		} else {
			getLogger().warn("The input port '{}' is not supported", port);
		}
	}
}