package quickstart;

import java.awt.BorderLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;

import javax.swing.JFrame;

import org.nlogo.awt.EventQueue;
import org.nlogo.lite.InterfaceComponent;
import org.nlogo.window.InvalidVersionException;
import org.nlogo.window.SpeedSliderPanel;
import org.nlogo.window.TickCounterLabel;

import mecsyco.core.model.ModelArtifact;
import mecsyco.core.type.SimulEvent;

public class RandomWalkModelArtifact extends ModelArtifact {

	protected JFrame frame;
	protected String frameName;
	protected int frameHeight, frameWidth;
	protected boolean guiStopped = false;

	protected String modelPath;
	protected double color;
	protected int size;
	protected InterfaceComponent interfaceComponent;

	// simulation
	protected int timeStep; // time discretization
	protected double lastEventTime = 0;


	public RandomWalkModelArtifact(String modelName, String modelPath, 
			int timeStep, 
			double color, int size, 
			int height, int width) {
		super(modelName);

		this.frame = new JFrame();	
		this.interfaceComponent = new InterfaceComponent(frame);

		this.frameName = modelName;
		this.modelPath = modelPath;

		this.frameHeight = height;
		this.frameWidth = width;

		this.timeStep = timeStep;

		this.color = color;
		this.size = size;
	}

	public void initialize() {
		try {
			EventQueue.invokeAndWait(new Runnable() {
				public void run() {
					frame.setSize(frameWidth, frameHeight);
					frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					frame.addWindowListener(new WindowAdapter() {
						@Override
						public void windowClosing(WindowEvent e) {
							super.windowClosing(e);
						}
					});

					frame.setTitle(frameName);
					frame.setLayout(new BorderLayout());
					frame.setContentPane(interfaceComponent);
					frame.setVisible(true);

					try {
						interfaceComponent.open(modelPath);
						TickCounterLabel t = new TickCounterLabel(interfaceComponent.world());
						SpeedSliderPanel s = new SpeedSliderPanel(interfaceComponent.workspace());
						frame.getContentPane().add(t, BorderLayout.SOUTH);
						frame.getContentPane().add(s, BorderLayout.NORTH);
					} catch (IOException e1) {
						e1.printStackTrace();
					} catch (InvalidVersionException e1) {
						e1.printStackTrace();
					}

				}
			});
		} catch (InterruptedException e) {
			e.printStackTrace();
		}	
	}

	public void setInitialParameters(String[] arg0) {
		interfaceComponent.command("set g-color " + color);
		interfaceComponent.command("set g-size " + size);
		interfaceComponent.command("setup");
	}

	public double getLastEventTime() {
		return lastEventTime;
	}

	public SimulEvent getExternalOutputEvent(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public double getNextInternalEventTime() {
		if (!guiStopped) {
			return lastEventTime + timeStep;
		}
		return Double.MAX_VALUE;
	}

	public void processExternalInputEvent(SimulEvent arg0, String arg1) {
		// TODO Auto-generated method stub

	}

	public void processInternalEvent(double time) {
		interfaceComponent.command("go");
		lastEventTime = time;
	}

	public void finishSimulation() {
		frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
	}
}