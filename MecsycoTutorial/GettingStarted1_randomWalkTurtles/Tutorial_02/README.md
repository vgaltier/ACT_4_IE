# Mecsyco Tutorial #2

Please read [Tutorial #1](https://gitlab.inria.fr/vgaltier/ACT_4_IE/-/tree/main/MecsycoTutorial/Tutorial_01) first as this example is built on it.

In Tutorial #1, we've used the `NetLogoModelArtifact.java` from the `mecsyco.world.netlogo` package in MecsycoJava.

In this example, we create a new ModelArtifact instead.
