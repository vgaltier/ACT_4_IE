package quickstart;

import mecsyco.core.agent.EventMAgent;
import mecsyco.core.coupling.CentralizedEventCouplingArtifact;
import mecsyco.core.model.ModelArtifact;
import mecsyco.core.operation.event.EventOperation;

public class Launcher {

	/**
	 * World name (used as frame title)
	 */
	public static final String WORLD_NAME = "Random Turtles World";
	/**
	 * Frame width
	 */
	public static final int WIDTH = 800;
	/**
	 * Frame height
	 */
	public static final int HEIGHT = 600;
	/**
	 * Turtles color
	 */
	public static final double COLOR = 15; //Red
	/**
	 * Turtles size
	 */
	public static final int SIZE = 2;
	/**
	 * Maximum simulation time
	 */
	public static final double MAX_SIMULATION_TIME = 10000;
	/**
	 * Time discretization
	 */
	public static final int TIME_STEP_SIZE = 1;
	/**
	 * Path to the NetLogo model
	 */
	public static final String NETLOGO_MODEL_PATH = "../random_walk.nlogo";


	public static void main(String[] args) {
		EventMAgent agent = new EventMAgent(WORLD_NAME, MAX_SIMULATION_TIME);

		
		ModelArtifact modelArtifact = new RandomWalkModelArtifactWithIO(WORLD_NAME, NETLOGO_MODEL_PATH,
				TIME_STEP_SIZE,
				COLOR, SIZE,
				HEIGHT, WIDTH);

		agent.setModelArtifact(modelArtifact);

		CentralizedEventCouplingArtifact couplingArtifact = new CentralizedEventCouplingArtifact();

		EventOperation colorSetter = new TurtleColorSetter();
		couplingArtifact.addEventOperation(colorSetter);
		
		agent.addInputCouplingArtifact(couplingArtifact, "in");
		agent.addOutputCouplingArtifact(couplingArtifact, "out");

		agent.startModelSoftware();
		agent.setModelParameters();
		agent.start();
	}
}



		

