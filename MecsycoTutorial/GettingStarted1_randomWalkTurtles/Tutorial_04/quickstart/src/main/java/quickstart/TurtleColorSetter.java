package quickstart;

import java.util.ArrayList;
import java.util.function.Consumer;

import org.nlogo.agent.Turtle;

import mecsyco.core.operation.event.EventOperation;
import mecsyco.core.type.SimulData;
import mecsyco.core.type.SimulEvent;
import mecsyco.world.netlogo.type.NetLogoColorConstants;

public class TurtleColorSetter extends EventOperation {

	@Override
	public SimulData apply(SimulEvent event) {
		SimulData data = event.getData();

		if (data instanceof HiddenWalkers) {
			final HiddenWalkers walkers = (HiddenWalkers) data;
			final HiddenWalkers newWalkers = new HiddenWalkers();
			ArrayList<Turtle> turtles = walkers.getWalkers();
			turtles.forEach(new Consumer<Turtle>() {
				public void accept(Turtle turtle) {
					Double currentColor = (Double) turtle.color();
					double newColor = nextColor(currentColor);
					turtle.colorDouble(newColor);
					newWalkers.add(turtle);
				}
			});
			return newWalkers;
		}
		return null;
	}

	private double nextColor(double currentColor) {
		final int[] colors = new int[] { NetLogoColorConstants.BLUE,
				NetLogoColorConstants.CYAN, NetLogoColorConstants.PINK, NetLogoColorConstants.ORANGE,
				NetLogoColorConstants.YELLOW, NetLogoColorConstants.GREEN, NetLogoColorConstants.RED };
		for (int i = 0; i < colors.length; i++) {
			if (currentColor == colors[i]) {
				return colors[(i + 1) % colors.length];
			}
		}
		return currentColor;
	}
}
