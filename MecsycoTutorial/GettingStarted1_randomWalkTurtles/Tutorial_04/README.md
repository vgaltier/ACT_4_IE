# Mecsyco Tutorial #4

Please read [Tutorial #3](https://gitlab.inria.fr/vgaltier/ACT_4_IE/-/tree/main/MecsycoTutorial/Tutorial_03) first as this example is built on it.

This example illustrates the notion of *operation*, a feature offered by Mecsyco to apply transformations to exchanged data.

In the previous NetLogo random walkers example, walkers that exit the world are spawn back at the origin in the same color as "initial" walkers. In this example, the color of exchanged walker is changed before they are transmitted to the model artifact.


## Adding an Operation

There are no needs to introduce a new model artifact or modify the existing one.

An operation is symbolized by a point and it is placed on a coupling artifact. It is applied to the data which cross the coupling artifact.

![Mecsyco model with an operation](images/single_random_walk_with_operation.svg)
*Mecsyco model with an operation*

To add an operation to a coupling artifact, simply create the operation (line 2 below) and add it (line 3 below):

```java
1. CentralizedEventCouplingArtifact couplingArtifact = new CentralizedEventCouplingArtifact();
2. EventOperation colorSetter = new TurtleColorSetter();
3. couplingArtifact.addEventOperation(colorSetter);
4. agent.addInputCouplingArtifact(couplingArtifact, "in");
5. agent.addOutputCouplingArtifact(couplingArtifact, "out");
```

We can add multiple operations to a single coupling artifact.

## Creating an Operation

To create an operation, extends the [```mecsyco.core.operation.event.EventOperation```](https://gitlab.inria.fr/Simbiot/mecsyco/mecsycojava/-/blob/developer/mecsyco/mecsyco-core/src/main/java/mecsyco/core/operation/event/EventOperation.java).

You need to implement the ```apply``` method. Its argument is the exchanged event. Typed as ```mecsyco.core.type.SimulEvent```, it consists of the time at which an event occurs and the associated exchanged data, typed as ```mecsyco.type.core.SimulVector```.

The code below looks up the color of each exchanged walker and change it to blue. When a red walker leaves the world again, its color is changed to blue, etc. untils the walker is colored in red again.

```java
	@Override
	public SimulData apply(SimulEvent event) {
		SimulData data = event.getData();

		if (data instanceof HiddenWalkers) {
			final HiddenWalkers walkers = (HiddenWalkers) data;
			final HiddenWalkers newWalkers = new HiddenWalkers();
			ArrayList<Turtle> turtles = walkers.getWalkers();
			turtles.forEach(new Consumer<Turtle>() {
				public void accept(Turtle turtle) {
					Double currentColor = (Double) turtle.color();
					double newColor = nextColor(currentColor);
					turtle.colorDouble(newColor);
					newWalkers.add(turtle);
				}
			});
			return newWalkers;
		}
		return null;
	}

	private double nextColor(double currentColor) {
		final int[] colors = new int[] { NetLogoColorConstants.BLUE,
				NetLogoColorConstants.CYAN, NetLogoColorConstants.PINK, NetLogoColorConstants.ORANGE,
				NetLogoColorConstants.YELLOW, NetLogoColorConstants.GREEN, NetLogoColorConstants.RED };
		for (int i = 0; i < colors.length; i++) {
			if (currentColor == colors[i]) {
				return colors[(i + 1) % colors.length];
			}
		}
		return currentColor;
	}

```

![Effect of the color setter operation](images/colored_walkers.png)
*Effect of the color setter operation*
