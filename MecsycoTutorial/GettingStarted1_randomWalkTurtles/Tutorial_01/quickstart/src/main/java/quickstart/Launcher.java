package quickstart;

import java.util.HashMap;
import java.util.Map;

import mecsyco.core.agent.EventMAgent;
import mecsyco.core.model.ModelArtifact;
import mecsyco.world.netlogo.NetLogoModelArtifact;
import mecsyco.world.netlogo.NetlogoInput;
import mecsyco.world.netlogo.NetlogoOutput;

public class Launcher {

	/**
	 * World name (used as frame title)
	 */
	public static final String WORLD_NAME = "Random Turtles World";
	/**
	 * Frame width
	 */
	public static final int WIDTH = 800;
	/**
	 * Frame height
	 */
	public static final int HEIGHT = 600;
	/**
	 * Turtles color
	 */
	public static final double COLOR = 15; //Red
	/**
	 * Turtles size
	 */
	public static final int SIZE = 2;
	/**
	 * Maximum simulation time
	 */
	public static final double MAX_SIMULATION_TIME = 1000;
	/**
	 * Time discretization
	 */
	public static final int TIME_STEP_SIZE = 1;
	/**
	 * Path to the NetLogo model
	 */
	public static final String NETLOGO_MODEL_PATH = "../random_walk.nlogo";


	public static void main(String[] args) {
		EventMAgent agent = new EventMAgent(WORLD_NAME, MAX_SIMULATION_TIME);

		Map<String, String> parameters = new HashMap<String, String>();
		// map of model parameters and corresponding instructions
		parameters.put("color","set g-color " + COLOR); 
		parameters.put("size", "set g-size " + SIZE);
		Map<String, Object> initVars = new HashMap<String, Object>();
		// map of initial values for parameters
		initVars.put("color", COLOR); 
		initVars.put("size", SIZE);
		Map<String, NetlogoInput> inputs = null;
		Map<String, NetlogoOutput> outputs = null;
		ModelArtifact modelArtifact = new NetLogoModelArtifact(WORLD_NAME, NETLOGO_MODEL_PATH,
				TIME_STEP_SIZE,
				parameters, inputs, outputs, initVars,
				HEIGHT, WIDTH);

		agent.setModelArtifact(modelArtifact);

		agent.startModelSoftware();
		agent.setModelParameters();
		agent.start();
	}
}
