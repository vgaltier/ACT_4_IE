# Mecsyco Tutorial #1

This minimal example illustrates how to launch a single model from Mecsyco.

## The Netlogo random walk model

The `random_walk.nlogo` model creates a pool of walkers – NetLogo turtles – positioned at the origin. At each step – NetLogo tick – the walkers move in a random direction. When a walker overpasses the border of the world and steps onto the gray ground, it is hidden. At each step, hidden walkers die. Therefore the count of walkers decreases over time.

![The NetLogo random walk model](images/netlogo_random_walk.png)

The Petri network below represents a walker’s state. Each place is a fulfilled condition and a transition is an event. Only one transition is playable per step.

![Petri network of a walker state](images/walker_state_petri_net.svg)

## Integration into Mecsyco

This minimal example only consists in launching the NetLogo model from a Mecsyco model. The Mecsyco model only consists in a *m-agent* and a *Model Artifact*.

![Random walk MECSYCO modeling](images/single_random_walk.svg)

The m-agent is a generic component shipped with the Mecsyco core (`EventMAgent`). It intercepts each step of the simulation it is in charge of, and communicates via the model artifact.

A model artifact is like the interface between the model instance under simulation and the m-agent. The model artifact is a domain-specific component. However, it is possible to have a simulator-dependent generic artifact. At this time, a generic version of the NetLogo model artifact, `NetLogoModelArtifact.java`, is available in the `mecsyco.world.netlogo` package in MecsycoJava.

`Launcher` instantiates this model. The m-agent is created with the duration of the simulation (`MAX_SIMULATION_TIME`; here, the duration quantifies the number of NetLogo ticks to perform). The model artifact is created with the path to the model.

```java 
EventMAgent agent = new EventMAgent(WORLD_NAME, MAX_SIMULATION_TIME);

Map<String, String> parameters = new HashMap<String, String>();
// map of model parameters and corresponding instructions
parameters.put("color","set g-color " + COLOR); 
parameters.put("size", "set g-size " + SIZE);
Map<String, Object> initVars = new HashMap<String, Object>();
// map of initial values for parameters
initVars.put("color", COLOR); 
initVars.put("size", SIZE);
Map<String, NetlogoInput> inputs = null;
Map<String, NetlogoOutput> outputs = null;
ModelArtifact modelArtifact = new NetLogoModelArtifact(WORLD_NAME, NETLOGO_MODEL_PATH,
		TIME_STEP_SIZE,
		parameters, inputs, outputs, initVars,
		HEIGHT, WIDTH);

agent.setModelArtifact(modelArtifact);

agent.startModelSoftware();
agent.setModelParameters();
agent.start();
```

