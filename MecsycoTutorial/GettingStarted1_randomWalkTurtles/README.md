# Mecsyco Tutorials

Based on [Getting Started](http://mecsyco.com/dev/doc/getting-started/).
This series uses a NetLogo model.

- [Tutorial #1](Tutorial_01): how to launch a model from Mecsyco
- [Tutorial #2](Tutorial_02): illustrates how to create a ModelArtifact
- [Tutorial #3](Tutorial_03): illustrates the notions of input and output *ports* of a model, and how to connect ports using a *coupling artifact*
- [Tutorial #4](Tutorial_04): illustrates the notion of *operation*, a feature offered by Mecsyco to apply transformations to exchanged data
- [Tutorial #5](Tutorial_05): illustrates the interaction between 2 models

Each tutorial comes with a Docker image that contains Mecsyco install, Eclipse IDE, and the source code of the tutorial. The container can be used as a remote desktop. 

How to (example for Tutorial #1, adapt the image name to the tutorial):
1. run the container: `docker run --rm --detach --publish 6080:80 registry.gitlab.inria.fr/vgaltier/act_4_ie/mecsyco_tutorial_1:latest`
2. use a web browser and visit [http://127.0.0.1:6080/](http://127.0.0.1:6080/)
3. start Eclipse (`eclipse`), and open the `/root/Desktop/quickstart` project

Notes: 
- sudo password is "ubuntu"
- if you want to "mount"/share a host directory to the `/workspace` directory in the container: `docker run --rm --detach --publish 6080:80 --volume YOUR_CHOICE_OF_PATH:/workspace:rw registry.gitlab.inria.fr/vgaltier/act_4_ie/mecsyco_tutorial_1:latest`
- to use your host user name and id (XXXX is given by `id -u` and YYYY by `id -n -u`): `docker run --rm --detach --publish 6080:80 --volume YOUR_CHOICE_OF_PATH:/workspace:rw --env USERNAME=YYYYYY --env USERID=XXXX registry.gitlab.inria.fr/vgaltier/act_4_ie/mecsyco_tutorial_1:latest`

