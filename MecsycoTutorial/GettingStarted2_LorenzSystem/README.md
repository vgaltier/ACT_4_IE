# Mecsyco Tutorial series#2

Based on [Getting Started 2](http://mecsyco.com/dev/doc/getting-started2/).

Instead of NetLogo components, this series uses the [Lorenz system](https://en.wikipedia.org/wiki/Lorenz_system) as an example.

- [Part #1](Part1) presents how to build and run a co-simulation
- [Part #2](Part2) presents MECSYCO components dedicated to the display of cosimulation results with different types of graph. 
