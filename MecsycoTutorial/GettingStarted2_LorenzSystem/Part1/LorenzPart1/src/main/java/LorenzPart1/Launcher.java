package LorenzPart1;

import mecsyco.core.agent.EventMAgent;
import mecsyco.core.coupling.CentralizedEventCouplingArtifact;
import model.LorenzX;
import model.LorenzY;
import model.LorenzZ;

public class Launcher {

	public static void main(String[] args) {
		// config of the simulation
		//-------------------------
		double maxSimulationTime = 0.5;
		double initX = 1, initY = 1, initZ = 4;
		double A = 10, B = 28, C = 2.67;

		// create the simulators
		/*
		// with an identical time step:		
		LorenzX simulatorX = new LorenzX(initX, A);
		LorenzY simulatorY = new LorenzY(initY, B);
		LorenzZ simulatorZ = new LorenzZ(initZ, C);
		*/
		// with different time steps:
		LorenzX simulatorX = new LorenzX(initX, A, 0.01);
		LorenzY simulatorY = new LorenzY(initY, B, 0.02);
		LorenzZ simulatorZ = new LorenzZ(initZ, C, 0.07);
		
		// create the model artifacts
		LorenzModelArtifact xModelArtifact = new LorenzModelArtifact("Model X", simulatorX);
		LorenzModelArtifact yModelArtifact = new LorenzModelArtifact("Model Y", simulatorY);
		LorenzModelArtifact zModelArtifact = new LorenzModelArtifact("Model Z", simulatorZ);

		// create the agents 
		EventMAgent agentX = new EventMAgent("agentX", maxSimulationTime);
		EventMAgent agentY = new EventMAgent("agentY", maxSimulationTime);
		EventMAgent agentZ = new EventMAgent("agentZ", maxSimulationTime);

		// associate agents with simulators
		agentX.setModelArtifact(xModelArtifact);
		agentY.setModelArtifact(yModelArtifact);
		agentZ.setModelArtifact(zModelArtifact);

		// create communication links between agents
		// x -> z
		CentralizedEventCouplingArtifact xOutputToZ = new CentralizedEventCouplingArtifact();
		agentX.addOutputCouplingArtifact(xOutputToZ, LorenzX.X);
		agentZ.addInputCouplingArtifact(xOutputToZ, LorenzZ.X);
		// x -> y
		CentralizedEventCouplingArtifact xOutputToY = new CentralizedEventCouplingArtifact();
		agentX.addOutputCouplingArtifact(xOutputToY, LorenzX.X);
		agentY.addInputCouplingArtifact(xOutputToY, LorenzY.X);
		// y -> x
		CentralizedEventCouplingArtifact yOutputToX = new CentralizedEventCouplingArtifact();
		agentY.addOutputCouplingArtifact(yOutputToX, LorenzY.Y);
		agentX.addInputCouplingArtifact(yOutputToX, LorenzX.Y);
		// y -> z
		CentralizedEventCouplingArtifact yOutputToZ = new CentralizedEventCouplingArtifact();
		agentY.addOutputCouplingArtifact(yOutputToZ, LorenzY.Y);
		agentZ.addInputCouplingArtifact(yOutputToZ, LorenzZ.Y);
		// z -> y
		CentralizedEventCouplingArtifact zOutputToY = new CentralizedEventCouplingArtifact();
		agentZ.addOutputCouplingArtifact(zOutputToY, LorenzZ.Z);
		agentY.addInputCouplingArtifact(zOutputToY, LorenzY.Z);

		// start the components
		agentX.startModelSoftware();
		agentY.startModelSoftware();
		agentZ.startModelSoftware();
		
		agentX.setModelParameters();
		agentY.setModelParameters();
		agentZ.setModelParameters();
		
		agentX.coInitialize();
		agentY.coInitialize();
		agentZ.coInitialize();

		agentX.start();
		agentY.start();
		agentZ.start();

	}
}
