package model;

public class LorenzZ extends Equation {

	public static final String C = "C";
	public static final String X = "X";
	public static final String Y = "Y";
	public static final String Z = "Z";

	public LorenzZ(double z, double c, double timeStep) {
		super("Zmodel", new String[] { X, Y, Z }, new double[] { 0, 0, z }, new String[] { C }, new double[] { c }, timeStep);
	}

	public LorenzZ(double z, double c) {
		this(z, c, DEFAULT_TIME_STEP);
	}

	@Override
	public void dynamics() {
		double dz = getVariable(X) * getVariable(Y) - getParameters(C) * getVariable(Z); 
		setVariable(Z, getVariable(Z) + dz * getTimeStep());
	}

}
