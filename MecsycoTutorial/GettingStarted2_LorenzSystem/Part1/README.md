## The Lorenz system model

We suppose 3 models [LorenzX](https://gitlab.inria.fr/vgaltier/ACT_4_IE/-/blob/main/MecsycoTutorial/GettingStarted2_LorenzSystem/Part1/LorenzPart1/src/main/java/model/LorenzX.java), [LorenzY](https://gitlab.inria.fr/vgaltier/ACT_4_IE/-/blob/main/MecsycoTutorial/GettingStarted2_LorenzSystem/Part1/LorenzPart1/src/main/java/model/LorenzY.java), and [LorenzZ](https://gitlab.inria.fr/vgaltier/ACT_4_IE/-/blob/main/MecsycoTutorial/GettingStarted2_LorenzSystem/Part1/LorenzPart1/src/main/java/model/LorenzZ.java), one for each variable `x`, `y`, and `z`; they are special cases of [Equation](https://gitlab.inria.fr/vgaltier/ACT_4_IE/-/blob/main/MecsycoTutorial/GettingStarted2_LorenzSystem/Part1/LorenzPart1/src/main/java/model/Equation.java), a simple differential equation Euler solver.

The equations depend on some initial parameters that are called `A`, `B`, and `C` here.

In addition to the computation method, they offer rudimentary trace functionalities to observe simulation: at each step of resolution, the solver prints in the console the current time step, the name of the model and the set of values of its variables:

```
0.01	Zmodel	{X=1.0, Y=1.0, Z=3.9032}
0.01	Xmodel	{X=1.0, Y=1.0}
0.01	Ymodel	{X=1.0, Y=1.23, Z=4.0}
0.02	Ymodel	{X=1.0, Y=1.458668, Z=3.9032}
0.02	Zmodel	{X=1.0, Y=1.23, Z=3.81128456}
0.02	Xmodel	{X=1.023, Y=1.23}
0.03	Xmodel	{X=1.0665668, Y=1.458668}
0.03	Ymodel	{X=1.023, Y=1.6915318789512002, Z=3.81128456}
0.03	Zmodel	{X=1.023, Y=1.458668, Z=3.7244454358879997}
0.04	Zmodel	{X=1.0665668, Y=1.6915318789512002, Z=3.6430440601821}
0.04	Xmodel	{X=1.12906330789512, Y=1.6915318789512002}
0.04	Ymodel	{X=1.0665668, Y=1.9335315656583916, Z=3.7244454358879997}
0.05	Xmodel	{X=1.209510133671447, Y=1.9335315656583916}
0.05	Ymodel	{X=1.12906330789512, Y=2.1892017024384725, Z=3.6430440601821}
0.05	Zmodel	{X=1.12906330789512, Y=1.9335315656583916, Z=3.5676055792296566}
```

## Programming the multi-model

The multi-model uses an agent for each of the 3 models.

Each simulator (differential equation solver) and its associated agent is defined by its inputs and outputs. For instance, AgentX outputs the value of the `x` variable. As inputs, it requires the value of `y`. To initialize the model, we also need the value of the parameter `a` and the initial value of `x`.

### The model artifact

A model artifact reifies interactions between a Mecsyco agent and a model (see [the section about the interface artifact in the User Guide](http://mecsyco.com/dev/doc/User%20Guide.pdf#subsection.2.3.3)). Defining such an artifact consists in extending [`ModelArtifact`](https://gitlab.inria.fr/Simbiot/mecsyco/mecsycojava/-/blob/developer/mecsyco/mecsyco-core/src/main/java/mecsyco/core/model/ModelArtifact.java).

[`LorenzModelArtifact`]() wraps an equation: when an external input event occurs, it forwards the received inputs to the model; when it's time to do so, it calls the `doStep` method; and it ouputs the value of `x`, `y`, or `z` (depending on the dynamic type of the wrapped equation).

### Set-up the multi-simulation

The steps to setting up a simulation are:
- create the simulators:
```java 
LorenzX simulatorX = new LorenzX(initX, A);
LorenzY simulatorY = new LorenzY(initY, B);
LorenzZ simulatorZ = new LorenzZ(initZ, C);
```
- create the model artifacts:
```java
LorenzModelArtifact xModelArtifact = new LorenzModelArtifact("Model X", simulatorX);
LorenzModelArtifact yModelArtifact = new LorenzModelArtifact("Model Y", simulatorY);
LorenzModelArtifact zModelArtifact = new LorenzModelArtifact("Model Z", simulatorZ);
```
- create the agents:
```java
EventMAgent agentX = new EventMAgent("agentX", maxSimulationTime);
EventMAgent agentY = new EventMAgent("agentY", maxSimulationTime);
EventMAgent agentZ = new EventMAgent("agentZ", maxSimulationTime);
```
- connect agents to simulators using model artifacts:
```java
agentX.setModelArtifact(xModelArtifact);
agentY.setModelArtifact(yModelArtifact);
agentZ.setModelArtifact(zModelArtifact);
```
- create communication links to connect agents: you need to create one link per directional communication (per arrow on the figure below); first instanciate the support `CentralizedEventCouplingArtifact` and link it to the output of an agent and the input of another agent, specifying the port name (in our models the name of the port, both in or out, is the same as the name of the value exchanged through that port)
![Exchanges](images/Lorenz.png)
```java
CentralizedEventCouplingArtifact xOutputToZ = new CentralizedEventCouplingArtifact();
agentX.addOutputCouplingArtifact(xOutputToZ, LorenzX.X);
agentZ.addInputCouplingArtifact(xOutputToZ, LorenzZ.X);
CentralizedEventCouplingArtifact xOutputToY = new CentralizedEventCouplingArtifact();
agentX.addOutputCouplingArtifact(xOutputToY, LorenzX.X);
agentY.addInputCouplingArtifact(xOutputToY, LorenzY.X);
CentralizedEventCouplingArtifact yOutputToX = new CentralizedEventCouplingArtifact();
agentY.addOutputCouplingArtifact(yOutputToX, LorenzY.Y);
agentX.addInputCouplingArtifact(yOutputToX, LorenzX.Y);
CentralizedEventCouplingArtifact yOutputToZ = new CentralizedEventCouplingArtifact();
agentY.addOutputCouplingArtifact(yOutputToZ, LorenzY.Y);
agentZ.addInputCouplingArtifact(yOutputToZ, LorenzZ.Z);
CentralizedEventCouplingArtifact zOutputToY = new CentralizedEventCouplingArtifact();
agentZ.addOutputCouplingArtifact(zOutputToY, LorenzZ.Z);
agentY.addInputCouplingArtifact(zOutputToY, LorenzY.Z);
```

The whole multi-model is now fully described.

### Launching the simulation

The agents start the simulator software. The next step would be to set the model parameters using the `setModelParameters` method but since in our example the initial values for the simulators are set in the constructor, we don't need it. Before we start we start the agents to begin the simulation, we use the `coInitialize` method so that each agent sends initial data to the others:

```java
agentX.startModelSoftware();
agentY.startModelSoftware();
agentZ.startModelSoftware();
	
agentX.setModelParameters();
agentY.setModelParameters();
agentZ.setModelParameters();
		
agentX.coInitialize();
agentY.coInitialize();
agentZ.coInitialize();		

agentX.start();
agentY.start();
agentZ.start();
```

## Simulation with different time steps

Our simulators have a second constructor that allows us to define the time step. It is then possible to have the three simulators with different time step, and MECSYCO will still manage synchronization without trouble. 

For instance, change the code to:

```java
LorenzX simulatorX = new LorenzX(initX, A, 0.01);
LorenzY simulatorY = new LorenzY(initY, B, 0.02);
LorenzZ simulatorZ = new LorenzZ(initZ, C, 0.07);
```

We obtain the following results:

```
0.01	Xmodel	{X=1.0, Y=1.0}
0.02	Xmodel	{X=1.0, Y=1.0}
0.02	Ymodel	{X=1.0, Y=1.46, Z=4.0}
0.03	Xmodel	{X=1.046, Y=1.46}
0.04	Xmodel	{X=1.0874, Y=1.46}
0.04	Ymodel	{X=1.046, Y=1.93288, Z=4.0}
0.05	Xmodel	{X=1.171948, Y=1.93288}
0.06	Ymodel	{X=1.171948, Y=2.45675744, Z=4.0}
0.060000000000000005	Xmodel	{X=1.300428944, Y=2.45675744}
0.07	Xmodel	{X=1.4160617936000002, Y=2.45675744}
0.07	Zmodel	{X=1.300428944, Y=2.45675744, Z=3.476038693835434}
0.08	Xmodel	{X=1.5201313582400002, Y=2.45675744}
0.08	Ymodel	{X=1.4160617936000002, Y=3.1021711838676884, Z=3.476038693835434}
0.09	Xmodel	{X=1.678335340802769, Y=3.1021711838676884}
0.09999999999999999	Xmodel	{X=1.820718925109261, Y=3.1021711838676884}
0.1	Ymodel	{X=1.820718925109261, Y=3.933152569565956, Z=3.476038693835434}
```

Explanation: 

At first, only the X model is active; but according to the Lorenz system equations, the variation of `X` is influenced by the difference between `X` et `Y`; since the X model doesn't know the new value of `Y` yet, there is no movement at time 0.01.

The Y model will only send its new value each 0.02 simulation time. Between these 0.02 updates (steps 0.03 and 0.04), the X model will evolve with its latest value of `Y` and `Y` won't change.

The same goes for `Z`: it will not influence `Y` not be influenced by `X` until 0.07.
