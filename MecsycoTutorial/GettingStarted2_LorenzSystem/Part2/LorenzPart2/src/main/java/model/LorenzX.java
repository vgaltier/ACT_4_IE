package model;

public class LorenzX extends Equation {

	public static final String A = "A";
	public static final String X = "X";
	public static final String Y = "Y";

	public LorenzX(double x, double a, double timeStep) {
		super("Xmodel", new String[] { X, Y }, new double[] { x, 0 }, new String[] { A }, new double[] { a }, timeStep);
	}

	public LorenzX(double x, double a) {
		this(x, a, DEFAULT_TIME_STEP);
	}

	@Override
	public void dynamics() {
		double dx = getParameters(A) * (getVariable(Y) - getVariable(X));
		setVariable(X, getVariable(X) + dx * getTimeStep());
	}
}
