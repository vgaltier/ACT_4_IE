package model;

public class LorenzY extends Equation {

	public static final String B = "B";
	public static final String X = "X";
	public static final String Y = "Y";
	public static final String Z = "Z";

	public LorenzY(double y, double b, double timeStep) {
		super("Ymodel", new String[] { X, Y, Z }, new double[] { 0, y, 0 }, new String[] { B }, new double[] { b }, timeStep);
	}

	public LorenzY(double y, double b) {
		this(y, b, DEFAULT_TIME_STEP);
	}

	@Override
	public void dynamics() {
		double dy = getVariable(X) * (getParameters(B) - getVariable(Z)) - getVariable(Y); 
		setVariable(Y, getVariable(Y) + dy * getTimeStep());
	}

}
