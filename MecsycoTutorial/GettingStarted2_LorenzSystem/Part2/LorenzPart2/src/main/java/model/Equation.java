package model;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

public abstract class Equation {
	/**
	 * Name of the equation.
	 */
	protected String name;

	/**
	 * Map of state variables.
	 */
	protected Map<String, Double> stateVariables;

	/**
	 * Parameters of the equation.
	 */
	protected Map<String, Double> parameters;

	/**
	 * Current time.
	 */
	protected double time;

	/**
	 * Time step used when solving the equation.
	 */
	protected double timeStep;

	/**
	 * Log or not result in the console.
	 */
	protected boolean trace = true;

	/**
	 * Default time step.
	 */
	public static final double DEFAULT_TIME_STEP = 0.01;

	/**
	 * 
	 * @param name
	 * @param varNames
	 * @param varInitValues
	 * @param paramNames
	 * @param paramValues
	 * @param timeStep
	 */
	public Equation(String name, String[] varNames, double[] varInitValues, String[] paramNames, double[] paramValues,
			double timeStep) {
		assertEquals(varNames.length, varInitValues.length);
		assertEquals(paramNames.length, paramValues.length);
		this.name = name;
		this.time = 0;
		this.timeStep = timeStep;
		this.stateVariables = new HashMap<String, Double>();
		for (int i = 0; i < varNames.length; i++) {
			stateVariables.put(varNames[i], varInitValues[i]);
		}
		this.parameters = new HashMap<String, Double>();
		for (int i = 0; i < paramNames.length; i++) {
			parameters.put(paramNames[i], paramValues[i]);
		}
	}

	/**
	 * Compute the dynamics and move the time one time step ahead.
	 */
	public void doStep() {
		dynamics();
		time += timeStep;
		if (trace) {
			System.out.println(time + "\t" + name + "\t" + stateVariables);
		}
	}

	/**
	 * Method where to define the dynamic part of the equation.
	 */
	public abstract void dynamics();

	/**
	 *
	 * @return {@link #name}.
	 */
	public String getName() {
		return name;
	}

	/**
	 *
	 * @param var State variable name.
	 * @return Its value.
	 */
	public double getVariable(String var) {
		return stateVariables.get(var);
	}

	/**
	 *
	 * @param var   State variable name.
	 * @param value New value.
	 */
	public void setVariable(String var, double value) {
		stateVariables.put(var, value);
	}

	/**
	 *
	 * @param var Parameter name.
	 * @return Its value.
	 */
	public double getParameters(String var) {
		return parameters.get(var);
	}

	/**
	 *
	 * @return {@link #time}
	 */
	public double getTime() {
		return time;
	}

	/**
	 *
	 * @return {@link #timeStep}
	 */
	public double getTimeStep() {
		return timeStep;
	}

	/**
	 *
	 * @return {@link #trace}
	 */
	public boolean isTrace() {
		return trace;
	}

	/**
	 *
	 * @param b True to enable, false to disable.
	 */
	public void setTrace(boolean b) {
		trace = b;
	}

}
