package LorenzPart2;

import mecsyco.core.agent.EventMAgent;
import mecsyco.core.agent.ObservingMAgent;
import mecsyco.core.coupling.CentralizedEventCouplingArtifact;
import mecsyco.observing.jfreechart.bar.LiveBarGraphic;
import mecsyco.observing.jfreechart.bar.PostMortemBarGraphic;
import mecsyco.observing.jfreechart.event.LiveEventGraphic;
import mecsyco.observing.jfreechart.event.PostMortemEventGraphic;
import mecsyco.observing.jfreechart.pie.LivePieGraphic;
import mecsyco.observing.jfreechart.pie.PostMortemPieGraphic;
import mecsyco.observing.jfreechart.xy.LiveTXGraphic;
import mecsyco.observing.jfreechart.xy.LiveXYGraphic;
import mecsyco.observing.jfreechart.xy.PostMortemTXGraphic;
import mecsyco.observing.jfreechart.xy.PostMortemXYGraphic;
import mecsyco.observing.jfreechart.xy.Renderer;
import mecsyco.observing.jzy3d.graphic.PostMortem3DGraphic;
import model.LorenzX;
import model.LorenzY;
import model.LorenzZ;

public class LauncherGraphExamples {

	public static void main(String[] args) {
		// config of the simulation
		//-------------------------
		double maxSimulationTime = 100;
		double initX = 1, initY = 1, initZ = 4;
		double A = 10, B = 28, C = 2.67;

		// create the simulators
		/*
		// with an identical time step:		
		LorenzX simulatorX = new LorenzX(initX, A);
		LorenzY simulatorY = new LorenzY(initY, B);
		LorenzZ simulatorZ = new LorenzZ(initZ, C);
		 */
		// with different time steps:
		LorenzX simulatorX = new LorenzX(initX, A, 0.01);
		LorenzY simulatorY = new LorenzY(initY, B, 0.02);
		LorenzZ simulatorZ = new LorenzZ(initZ, C, 0.07);

		// create the model artifacts
		LorenzModelArtifact xModelArtifact = new LorenzModelArtifact("Model X", simulatorX);
		LorenzModelArtifact yModelArtifact = new LorenzModelArtifact("Model Y", simulatorY);
		LorenzModelArtifact zModelArtifact = new LorenzModelArtifact("Model Z", simulatorZ);

		// create the agents 
		EventMAgent agentX = new EventMAgent("agentX", maxSimulationTime);
		EventMAgent agentY = new EventMAgent("agentY", maxSimulationTime);
		EventMAgent agentZ = new EventMAgent("agentZ", maxSimulationTime);

		// associate agents with simulators
		agentX.setModelArtifact(xModelArtifact);
		agentY.setModelArtifact(yModelArtifact);
		agentZ.setModelArtifact(zModelArtifact);

		// create communication links between agents
		// x -> z
		CentralizedEventCouplingArtifact xOutputToZ = new CentralizedEventCouplingArtifact();
		agentX.addOutputCouplingArtifact(xOutputToZ, LorenzX.X);
		agentZ.addInputCouplingArtifact(xOutputToZ, LorenzZ.X);
		// x -> y
		CentralizedEventCouplingArtifact xOutputToY = new CentralizedEventCouplingArtifact();
		agentX.addOutputCouplingArtifact(xOutputToY, LorenzX.X);
		agentY.addInputCouplingArtifact(xOutputToY, LorenzY.X);
		// y -> x
		CentralizedEventCouplingArtifact yOutputToX = new CentralizedEventCouplingArtifact();
		agentY.addOutputCouplingArtifact(yOutputToX, LorenzY.Y);
		agentX.addInputCouplingArtifact(yOutputToX, LorenzX.Y);
		// y -> z
		CentralizedEventCouplingArtifact yOutputToZ = new CentralizedEventCouplingArtifact();
		agentY.addOutputCouplingArtifact(yOutputToZ, LorenzY.Y);
		agentZ.addInputCouplingArtifact(yOutputToZ, LorenzZ.Y);
		// z -> y
		CentralizedEventCouplingArtifact zOutputToY = new CentralizedEventCouplingArtifact();
		agentZ.addOutputCouplingArtifact(zOutputToY, LorenzZ.Z);
		agentY.addInputCouplingArtifact(zOutputToY, LorenzY.Z);

		// PostMortemXYGraphic
		ObservingMAgent postMortemXYGraphicObservingAgent = new ObservingMAgent("Lorenz observer", maxSimulationTime);
		PostMortemXYGraphic postMortemXYGraphic = new PostMortemXYGraphic("Lorenz", "X", "Y", Renderer.Line, "XY", "XY");
		postMortemXYGraphicObservingAgent.setDispatcherArtifact(postMortemXYGraphic);
		CentralizedEventCouplingArtifact xOutputToObs = new CentralizedEventCouplingArtifact();
		agentX.addOutputCouplingArtifact(xOutputToObs, "obs2d");
		postMortemXYGraphicObservingAgent.addInputCouplingArtifact(xOutputToObs, "XY");
	
		// LiveXYGraphic
		/*
		ObservingMAgent liveXYGraphicObservingAgent = new ObservingMAgent("Lorenz observer", maxSimulationTime);
		LiveXYGraphic liveXYGraphic = new LiveXYGraphic("Lorenz", "X", "Y", Renderer.Line, "XY", "XY");
		liveXYGraphicObservingAgent.setDispatcherArtifact(liveXYGraphic);
		CentralizedEventCouplingArtifact xOutputToObs = new CentralizedEventCouplingArtifact();
		agentX.addOutputCouplingArtifact(xOutputToObs, "obs2d");
		liveXYGraphicObservingAgent.addInputCouplingArtifact(xOutputToObs, "XY");
		 */

		// PostMortemTXGraphic
		/*
		ObservingMAgent postMortemTXGraphicObservingAgent = new ObservingMAgent("Lorenz observer", maxSimulationTime);
		PostMortemTXGraphic postMortemTXGraphic = new PostMortemTXGraphic("Lorenz", "value", Renderer.Line, 
				new String[] {"X series", "Y series", "Z series"}, 
				new String[] {"X", "Y", "Z"});
		postMortemTXGraphicObservingAgent.setDispatcherArtifact(postMortemTXGraphic);
		CentralizedEventCouplingArtifact xOutputToObs = new CentralizedEventCouplingArtifact();
		agentX.addOutputCouplingArtifact(xOutputToObs, "X");
		postMortemTXGraphicObservingAgent.addInputCouplingArtifact(xOutputToObs, "X");
		CentralizedEventCouplingArtifact yOutputToObs = new CentralizedEventCouplingArtifact();
		agentY.addOutputCouplingArtifact(yOutputToObs, "Y");
		postMortemTXGraphicObservingAgent.addInputCouplingArtifact(yOutputToObs, "Y");
		CentralizedEventCouplingArtifact zOutputToObs = new CentralizedEventCouplingArtifact();
		agentZ.addOutputCouplingArtifact(zOutputToObs, "Z");
		postMortemTXGraphicObservingAgent.addInputCouplingArtifact(zOutputToObs, "Z");
		 */

		// LiveMortemTXGraphic
		/*
		ObservingMAgent liveTXGraphicObservingAgent = new ObservingMAgent("Lorenz observer", maxSimulationTime);
		LiveTXGraphic liveTXGraphic = new LiveTXGraphic("Lorenz", "value", Renderer.Line, 
				new String[] {"X series", "Y series", "Z series"}, 
				new String[] {"X", "Y", "Z"});
		liveTXGraphicObservingAgent.setDispatcherArtifact(liveTXGraphic);
		CentralizedEventCouplingArtifact xOutputToObs = new CentralizedEventCouplingArtifact();
		agentX.addOutputCouplingArtifact(xOutputToObs, "X");
		liveTXGraphicObservingAgent.addInputCouplingArtifact(xOutputToObs, "X");
		CentralizedEventCouplingArtifact yOutputToObs = new CentralizedEventCouplingArtifact();
		agentY.addOutputCouplingArtifact(yOutputToObs, "Y");
		liveTXGraphicObservingAgent.addInputCouplingArtifact(yOutputToObs, "Y");
		CentralizedEventCouplingArtifact zOutputToObs = new CentralizedEventCouplingArtifact();
		agentZ.addOutputCouplingArtifact(zOutputToObs, "Z");
		liveTXGraphicObservingAgent.addInputCouplingArtifact(zOutputToObs, "Z");
		 */

		// PostMortemBarGraphic
		/*
		ObservingMAgent postMortemBarGraphicObservingAgent = new ObservingMAgent("Lorenz observer", maxSimulationTime);
		PostMortemBarGraphic postMortemBarGraphic = new PostMortemBarGraphic("Lorenz", "bars", "values", 
				new String[]{"X","Y","Z"}, "XYZ");
		postMortemBarGraphicObservingAgent.setDispatcherArtifact(postMortemBarGraphic);
		CentralizedEventCouplingArtifact zOutputToObs = new CentralizedEventCouplingArtifact();
		agentZ.addOutputCouplingArtifact(zOutputToObs, "vector3");
		postMortemBarGraphicObservingAgent.addInputCouplingArtifact(zOutputToObs, "XYZ");
		 */

		// LiveBarGraphic
		/*
		ObservingMAgent liveBarGraphicObservingAgent = new ObservingMAgent("Lorenz observer", maxSimulationTime);
		LiveBarGraphic liveBarGraphic = new LiveBarGraphic("Lorenz", "bars", "values", 
				new String[]{"X","Y","Z"}, "XYZ");
		liveBarGraphicObservingAgent.setDispatcherArtifact(liveBarGraphic);
		CentralizedEventCouplingArtifact zOutputToObs = new CentralizedEventCouplingArtifact();
		agentZ.addOutputCouplingArtifact(zOutputToObs, "vector3");
		liveBarGraphicObservingAgent.addInputCouplingArtifact(zOutputToObs, "XYZ");
		 */

		// PostMortemPieGraphic
		/*
		ObservingMAgent postMortemPieGraphicObservingAgent = new ObservingMAgent("Lorenz observer", maxSimulationTime);
		PostMortemPieGraphic postMortemPieGraphic = new PostMortemPieGraphic("Lorenz", 
				new String[]{"X","Y","Z"}, "XYZ");
		postMortemPieGraphicObservingAgent.setDispatcherArtifact(postMortemPieGraphic);
		CentralizedEventCouplingArtifact zOutputToObs = new CentralizedEventCouplingArtifact();
		agentZ.addOutputCouplingArtifact(zOutputToObs, "vector3");
		postMortemPieGraphicObservingAgent.addInputCouplingArtifact(zOutputToObs, "XYZ");
		 */

		// LivePieGraphic
		/*
		ObservingMAgent livePieGraphicObservingAgent = new ObservingMAgent("Lorenz observer", maxSimulationTime);
		LivePieGraphic livePieGraphic = new LivePieGraphic("Lorenz", 
				new String[]{"X","Y","Z"}, "XYZ");
		livePieGraphicObservingAgent.setDispatcherArtifact(livePieGraphic);
		CentralizedEventCouplingArtifact zOutputToObs = new CentralizedEventCouplingArtifact();
		agentZ.addOutputCouplingArtifact(zOutputToObs, "vector3");
		livePieGraphicObservingAgent.addInputCouplingArtifact(zOutputToObs, "XYZ");
		*/
		
		// PostMortemEventGraphic
		/*
		ObservingMAgent postMortemEventGraphicObservingAgent = new ObservingMAgent("Lorenz observer", maxSimulationTime);
		PostMortemEventGraphic postMortemEventGraphic = new PostMortemEventGraphic("Lorenz", "time", "X", "X", "X");
		postMortemEventGraphicObservingAgent.setDispatcherArtifact(postMortemEventGraphic);
		CentralizedEventCouplingArtifact xOutputToObs = new CentralizedEventCouplingArtifact();
		agentX.addOutputCouplingArtifact(xOutputToObs, "X");
		postMortemEventGraphicObservingAgent.addInputCouplingArtifact(xOutputToObs, "X");
		*/
		
		// LiveEventGraphic
		/*
		ObservingMAgent liveEventGraphicObservingAgent = new ObservingMAgent("Lorenz observer", maxSimulationTime);
		LiveEventGraphic liveEventGraphic = new LiveEventGraphic("Lorenz", "time", "X", "X", "X");
		liveEventGraphicObservingAgent.setDispatcherArtifact(liveEventGraphic);
		CentralizedEventCouplingArtifact xOutputToObs = new CentralizedEventCouplingArtifact();
		agentX.addOutputCouplingArtifact(xOutputToObs, "X");
		liveEventGraphicObservingAgent.addInputCouplingArtifact(xOutputToObs, "X");
		*/
		
		// PostMortem3DGraphic
		/*
		ObservingMAgent postMortem3DGraphicObservingAgent = new ObservingMAgent("Lorenz observer", maxSimulationTime);
		PostMortem3DGraphic postMortem3DGraphic = new PostMortem3DGraphic("Lorenz", "X", "Y", "Z", "XYZ");
		postMortem3DGraphicObservingAgent.setDispatcherArtifact(postMortem3DGraphic);
		CentralizedEventCouplingArtifact zOutputToObs = new CentralizedEventCouplingArtifact();
		agentZ.addOutputCouplingArtifact(zOutputToObs, "obs3d");
		postMortem3DGraphicObservingAgent.addInputCouplingArtifact(zOutputToObs, "XYZ");
		*/
		
		// start the components
		agentX.startModelSoftware();
		agentY.startModelSoftware();
		agentZ.startModelSoftware();

		postMortemXYGraphicObservingAgent.startModelSoftware();
		//liveXYGraphicObservingAgent.startModelSoftware();
		//postMortemTXGraphicObservingAgent.startModelSoftware();
		//liveTXGraphicObservingAgent.startModelSoftware();
		//postMortemBarGraphicObservingAgent.startModelSoftware();
		//liveBarGraphicObservingAgent.startModelSoftware();
		//postMortemPieGraphicObservingAgent.startModelSoftware();
		//livePieGraphicObservingAgent.startModelSoftware();
		//postMortemEventGraphicObservingAgent.startModelSoftware();
		//liveEventGraphicObservingAgent.startModelSoftware();
		//postMortem3DGraphicObservingAgent.startModelSoftware();
		
		agentX.setModelParameters();
		agentY.setModelParameters();
		agentZ.setModelParameters();

		agentX.coInitialize();
		agentY.coInitialize();
		agentZ.coInitialize();

		agentX.start();
		agentY.start();
		agentZ.start();

		postMortemXYGraphicObservingAgent.start();
		//liveXYGraphicObservingAgent.start();
		//postMortemTXGraphicObservingAgent.start();
		//liveTXGraphicObservingAgent.start();
		//postMortemBarGraphicObservingAgent.start();
		//liveBarGraphicObservingAgent.start();
		//postMortemPieGraphicObservingAgent.start();
		//livePieGraphicObservingAgent.start();
		//postMortemEventGraphicObservingAgent.start();
		//liveEventGraphicObservingAgent.start();
		//postMortem3DGraphicObservingAgent.start();
	}
}
