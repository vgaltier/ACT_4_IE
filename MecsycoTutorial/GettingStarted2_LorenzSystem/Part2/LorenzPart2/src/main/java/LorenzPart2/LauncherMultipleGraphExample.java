package LorenzPart2;

import mecsyco.core.agent.EventMAgent;
import mecsyco.core.agent.ObservingMAgent;
import mecsyco.core.coupling.CentralizedEventCouplingArtifact;
import mecsyco.observing.jfreechart.bar.LiveBarGraphic;
import mecsyco.observing.jfreechart.bar.PostMortemBarGraphic;
import mecsyco.observing.jfreechart.event.LiveEventGraphic;
import mecsyco.observing.jfreechart.event.PostMortemEventGraphic;
import mecsyco.observing.jfreechart.pie.LivePieGraphic;
import mecsyco.observing.jfreechart.pie.PostMortemPieGraphic;
import mecsyco.observing.jfreechart.xy.LiveTXGraphic;
import mecsyco.observing.jfreechart.xy.LiveXYGraphic;
import mecsyco.observing.jfreechart.xy.PostMortemTXGraphic;
import mecsyco.observing.jfreechart.xy.PostMortemXYGraphic;
import mecsyco.observing.jfreechart.xy.Renderer;
import mecsyco.observing.jzy3d.graphic.PostMortem3DGraphic;
import mecsyco.observing.swing.dispatcher.SwingDispatcherArtifact;
import model.LorenzX;
import model.LorenzY;
import model.LorenzZ;

public class LauncherMultipleGraphExample {

	public static void main(String[] args) {
		// config of the simulation
		//-------------------------
		double maxSimulationTime = 100;
		double initX = 1, initY = 1, initZ = 4;
		double A = 10, B = 28, C = 2.67;

		// create the simulators
		/*
		// with an identical time step:		
		LorenzX simulatorX = new LorenzX(initX, A);
		LorenzY simulatorY = new LorenzY(initY, B);
		LorenzZ simulatorZ = new LorenzZ(initZ, C);
		 */
		// with different time steps:
		LorenzX simulatorX = new LorenzX(initX, A, 0.01);
		LorenzY simulatorY = new LorenzY(initY, B, 0.02);
		LorenzZ simulatorZ = new LorenzZ(initZ, C, 0.07);

		// create the model artifacts
		LorenzModelArtifact xModelArtifact = new LorenzModelArtifact("Model X", simulatorX);
		LorenzModelArtifact yModelArtifact = new LorenzModelArtifact("Model Y", simulatorY);
		LorenzModelArtifact zModelArtifact = new LorenzModelArtifact("Model Z", simulatorZ);

		// create the agents 
		EventMAgent agentX = new EventMAgent("agentX", maxSimulationTime);
		EventMAgent agentY = new EventMAgent("agentY", maxSimulationTime);
		EventMAgent agentZ = new EventMAgent("agentZ", maxSimulationTime);

		// associate agents with simulators
		agentX.setModelArtifact(xModelArtifact);
		agentY.setModelArtifact(yModelArtifact);
		agentZ.setModelArtifact(zModelArtifact);

		// create communication links between agents
		// x -> z
		CentralizedEventCouplingArtifact xOutputToZ = new CentralizedEventCouplingArtifact();
		agentX.addOutputCouplingArtifact(xOutputToZ, LorenzX.X);
		agentZ.addInputCouplingArtifact(xOutputToZ, LorenzZ.X);
		// x -> y
		CentralizedEventCouplingArtifact xOutputToY = new CentralizedEventCouplingArtifact();
		agentX.addOutputCouplingArtifact(xOutputToY, LorenzX.X);
		agentY.addInputCouplingArtifact(xOutputToY, LorenzY.X);
		// y -> x
		CentralizedEventCouplingArtifact yOutputToX = new CentralizedEventCouplingArtifact();
		agentY.addOutputCouplingArtifact(yOutputToX, LorenzY.Y);
		agentX.addInputCouplingArtifact(yOutputToX, LorenzX.Y);
		// y -> z
		CentralizedEventCouplingArtifact yOutputToZ = new CentralizedEventCouplingArtifact();
		agentY.addOutputCouplingArtifact(yOutputToZ, LorenzY.Y);
		agentZ.addInputCouplingArtifact(yOutputToZ, LorenzZ.Y);
		// z -> y
		CentralizedEventCouplingArtifact zOutputToY = new CentralizedEventCouplingArtifact();
		agentZ.addOutputCouplingArtifact(zOutputToY, LorenzZ.Z);
		agentY.addInputCouplingArtifact(zOutputToY, LorenzY.Z);

		// Dispatcher
		ObservingMAgent multipleGraphicObservingAgent = new ObservingMAgent("Lorenz observer", maxSimulationTime);
		SwingDispatcherArtifact swingDispatcherArtifact = new SwingDispatcherArtifact();
		multipleGraphicObservingAgent.setDispatcherArtifact(swingDispatcherArtifact);
		
		// Attach observers to the dispatcher
		// PostMortemXYGraphic
		
		PostMortemXYGraphic postMortemXYGraphic = new PostMortemXYGraphic("Lorenz XY", "X", "Y", Renderer.Line, "XY", "XY");
		swingDispatcherArtifact.addObservingArtifact(postMortemXYGraphic);
		CentralizedEventCouplingArtifact xOutputToObs2d = new CentralizedEventCouplingArtifact();
		agentX.addOutputCouplingArtifact(xOutputToObs2d, "obs2d");
		multipleGraphicObservingAgent.addInputCouplingArtifact(xOutputToObs2d, "XY");
		
		// PostMortemTXGraphic
		
		PostMortemTXGraphic postMortemTXGraphic = new PostMortemTXGraphic("Lorenz TX", "value", Renderer.Line, 
				new String[] {"X series", "Y series", "Z series"}, 
				new String[] {"X", "Y", "Z"});
		swingDispatcherArtifact.addObservingArtifact(postMortemTXGraphic);
		CentralizedEventCouplingArtifact xOutputToObs = new CentralizedEventCouplingArtifact();
		agentX.addOutputCouplingArtifact(xOutputToObs, "X");
		multipleGraphicObservingAgent.addInputCouplingArtifact(xOutputToObs, "X");
		CentralizedEventCouplingArtifact yOutputToObs = new CentralizedEventCouplingArtifact();
		agentY.addOutputCouplingArtifact(yOutputToObs, "Y");
		multipleGraphicObservingAgent.addInputCouplingArtifact(yOutputToObs, "Y");
		CentralizedEventCouplingArtifact zOutputToObs = new CentralizedEventCouplingArtifact();
		agentZ.addOutputCouplingArtifact(zOutputToObs, "Z");
		multipleGraphicObservingAgent.addInputCouplingArtifact(zOutputToObs, "Z");
		
		// PostMortemPieGraphic
		PostMortemPieGraphic postMortemPieGraphic = new PostMortemPieGraphic("Lorenz Pie", 
				new String[]{"X","Y","Z"}, "XYZ");
		swingDispatcherArtifact.addObservingArtifact(postMortemPieGraphic);
		CentralizedEventCouplingArtifact zOutputToObsV3 = new CentralizedEventCouplingArtifact();
		agentZ.addOutputCouplingArtifact(zOutputToObsV3, "vector3");
		multipleGraphicObservingAgent.addInputCouplingArtifact(zOutputToObsV3, "XYZ");
		 
		// start the components
		agentX.startModelSoftware();
		agentY.startModelSoftware();
		agentZ.startModelSoftware();

		multipleGraphicObservingAgent.startModelSoftware();
		
		agentX.setModelParameters();
		agentY.setModelParameters();
		agentZ.setModelParameters();

		agentX.coInitialize();
		agentY.coInitialize();
		agentZ.coInitialize();

		agentX.start();
		agentY.start();
		agentZ.start();

		multipleGraphicObservingAgent.start();
	}
}
