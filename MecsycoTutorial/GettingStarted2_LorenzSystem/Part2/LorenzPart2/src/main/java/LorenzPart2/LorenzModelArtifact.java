package LorenzPart2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mecsyco.core.exception.UnexpectedTypeException;
import mecsyco.core.model.ModelArtifact;
import mecsyco.core.type.ArrayedSimulVector;
import mecsyco.core.type.SimulData;
import mecsyco.core.type.SimulEvent;
import mecsyco.core.type.Tuple1;
import mecsyco.core.type.Tuple2;
import mecsyco.core.type.Tuple3;
import model.Equation;

public class LorenzModelArtifact extends ModelArtifact {

	/**
	 * Equation solver (model) to wrap
	 */
	protected final Equation equation;

	protected final transient Logger modelLogger;

	public LorenzModelArtifact(String name, Equation equation) {
		super(name);
		this.equation = equation;
		this.modelLogger = LoggerFactory.getLogger(equation.getName());
	}

	public void initialize() {
	}

	public void setInitialParameters(String[] arg0) {
	}

	public void finishSimulation() {
		modelLogger.info("simulation end at time " + equation.getTime());
	}

	public double getLastEventTime() {
		return equation.getTime();
	}

	public double getNextInternalEventTime() {
		return equation.getTime() + equation.getTimeStep();
	}

	public void processInternalEvent(double arg0) {
		equation.doStep();
	}

	/**
	 * get the model output, to exchange it with another model through the named
	 * port
	 */
	public SimulEvent getExternalOutputEvent(String port) {
		if (port.equals("X") || port.equals("Y") || port.equals("Z")) {
			SimulData data = new Tuple1<>(equation.getVariable(port));
			SimulEvent result = new SimulEvent(data, equation.getTime());
			return result;
		} else if (port.equals("obs2d")) {
			SimulEvent result = new SimulEvent(new Tuple2<Double, Double>(equation.getVariable("X"), equation.getVariable("Y")), equation.getTime());
			return result;
		} else if (port.equals("obs3d")) {
			SimulEvent result = new SimulEvent(new Tuple3<Double, Double, Double>(equation.getVariable("X"), equation.getVariable("Y"), equation.getVariable("Z")), equation.getTime());
			return result;
		} else if (port.equals("vector3")) {
			SimulEvent result = new SimulEvent(new ArrayedSimulVector<>(equation.getVariable("X"), equation.getVariable("Y"), equation.getVariable("Z")), equation.getTime());
			return result;	
		} else {
			modelLogger.error("getExternalOutputEvent: unknown port " + port);
			System.exit(0);
		}
		return null;
	}

	/**
	 * set the model input according to the data received on the named input port
	 */
	public void processExternalInputEvent(SimulEvent simulEvent, String port) {
		SimulData eventData = simulEvent.getData();
		if (eventData instanceof Tuple1) {
			Tuple1<?> data = (Tuple1<?>) eventData;
			if (data.getItem1() instanceof Number) {
				if (port.equals("X") || port.equals("Y") || port.equals("Z")) {
					equation.setVariable(port, ((Number)(data.getItem1())).doubleValue());
				} else {
					modelLogger.error("processExternalInputEvent: unknown port " + port);
					System.exit(0);
				}
			} else {
				modelLogger.error(
						"processExternalInputEvent: " + new UnexpectedTypeException(Number.class, data.getItem1().getClass()));
				System.exit(0);
			}
		} else {
			modelLogger.error(
					"processExternalInputEvent: " + new UnexpectedTypeException(Tuple1.class, eventData.getClass()));
			System.exit(0);
		}
	}
}
