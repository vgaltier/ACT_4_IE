package LorenzPart2;

import mecsyco.core.type.SimulData;

@SuppressWarnings("serial")
public class Data implements SimulData {

	private Double value;

	public Data(Double value) {
		super();
		this.value = value;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}
}
