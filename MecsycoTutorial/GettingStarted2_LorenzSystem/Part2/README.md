# Adding observers to simulation

Mecysco offers a set of agents and artifacts dedicated to the display of the results from simulations with different types of graph. It also contains artifacts used for post-processing. The observer agent has the same behavior than any other MECSYCO agent, which makes its use and parametrization easier.

For more details, see [the MECSYCO-visu User Guide](http://mecsyco.com/dev/doc/User%20Guide%20MECSYCO-visu.pdf).

## Basic example

The first step to add an observer, is to create an observing agent.

To create an `ObservingMAgent` you need to provide a name (used when displaying information in the console) and the maximum time of the multi-simulation. Optionally, you provide the associated `ObservingArtifact`. If you don't provide the `ObservingArtifact` when you create the `ObservingMAgent`, you need to define it later using the `setDispatcherArtifact` method. 

MECSYCO proposes 6 types of graph and various post-processing functions which are covered in detail in the next sections. In this first example, we use the `PostMortemXYGraphic` ObservingArtifact that plots y=f(x) after the simulation is finished.

Its constructor requires 6 arguments:

- the title of the graph
- the name of the x axis
- the name of the y axis
- the renderer used (`Renderer.Line`, `Renderer.Dot` or `Renderer.Step`)
- the name of the series
- the name of the observed port

| ![Renderer.Line](images/Renderer.Line.png) | ![Renderer.Dot](images/Renderer.Dot.png) | ![Renderer.Step](images/Renderer.Step.png) |
|:--:|:--:|:--:|
| *Renderer.Line* | *Renderer.Dot* | *Renderer.Step* |

```java
ObservingMAgent obs = new ObservingMAgent("Lorenz observer", maxSimulationTime);
PostMortemXYGraphic graph = new PostMortemXYGraphic("Lorenz", "X", "Y", Renderer.Line, "XY", "XY");
obs.setDispatcherArtifact(graph);
```

Some observing artifacts need specific `SimulData`. That's the case of `PostMortemXYGraphic` that works with [`Tuple2`](https://gitlab.inria.fr/Simbiot/mecsyco/mecsycojava/-/blob/developer/mecsyco/mecsyco-core/src/main/java/mecsyco/core/type/Tuple2.java). So in the `LorenzModelArtifact` we create a specific port (named "obs2d") to export the expected `SimulData`:

```java
public SimulEvent getExternalOutputEvent(String port) {
	if (port.equals("X") || port.equals("Y") || port.equals("Z")) {
		Data data = new Data(equation.getVariable(port));
		SimulEvent result = new SimulEvent(data, equation.getTime());
		return result;
	} else if (port.equals("obs2d")) {
		SimulEvent result = new SimulEvent(new Tuple2<Double, Double>(equation.getVariable("X"), equation.getVariable("Y")), equation.getTime());
		return result;
	} else {
		modelLogger.error("getExternalOutputEvent: unknown port " + port);
		System.exit(0);
	}
	return null;
}
```

The last step is to define what to observe (which ports to observe). The links between the observing agent and the model agents is established with a coupling artifact as usual:

```java
CentralizedEventCouplingArtifact xOutputToObs = new CentralizedEventCouplingArtifact();
agentX.addOutputCouplingArtifact(xOutputToObs, "obs2d");
obs.addInputCouplingArtifact(xOutputToObs, "XY");
```		

Don't forget to start the observer:

```java
obs.startModelSoftware();
obs.start();
```

The complete code is available in [`LauncherBasicExample.java`](https://gitlab.inria.fr/vgaltier/ACT_4_IE/-/blob/main/MecsycoTutorial/GettingStarted2_LorenzSystem/Part2/LorenzPart2/src/main/java/LorenzPart2/LauncherBasicExample.java).

The displayed graph:

![PostMortemXYGraph](images/PostMortemXYGraph.png)

## The different graphs

### LiveXYGraphic and PostMortemXYGraphic

See the section above.

### LiveTXGraphic and PostMortermTXGraphic

TXGraphics are artifacts for temporal graphs. Their constructors requires 5 arguments:

- the title of the graph
- the name of the Y axis
- the Renderer to use
- the list of the names of the series
- the list of the names of the ports to observe

Note:

- It is possible to add as many series as desired, but too many can cause the simulation to slow down a bit.
- TXSeries only manage real values (Double) and expect Tuple1 SimulData.

```java
public void processExternalInputEvent(SimulEvent simulEvent, String port) {
	SimulData eventData = simulEvent.getData();
	if (eventData instanceof Tuple1) {
		Tuple1<?> data = (Tuple1<?>) eventData;
		if (data.getItem1() instanceof Number) {
			if (port.equals("X") || port.equals("Y") || port.equals("Z")) {
				equation.setVariable(port, ((Number)(data.getItem1())).doubleValue());
			} else {
				modelLogger.error("processExternalInputEvent: unknown port " + port);
				System.exit(0);
			}
		} else {
			modelLogger.error(
					"processExternalInputEvent: " + new UnexpectedTypeException(Number.class, data.getItem1().getClass()));
			System.exit(0);
		}
	} else {
		modelLogger.error(
				"processExternalInputEvent: " + new UnexpectedTypeException(Tuple1.class, eventData.getClass()));
		System.exit(0);
	}
}
```
---

```java
ObservingMAgent postMortemTXGraphicObservingAgent = new ObservingMAgent("Lorenz observer", maxSimulationTime);
PostMortemTXGraphic postMortemTXGraphic = new PostMortemTXGraphic("Lorenz", "value", Renderer.Line, 
		new String[] {"X series", "Y series", "Z series"}, 
		new String[] {"X", "Y", "Z"});
postMortemTXGraphicObservingAgent.setDispatcherArtifact(postMortemTXGraphic);
CentralizedEventCouplingArtifact xOutputToObs = new CentralizedEventCouplingArtifact();
agentX.addOutputCouplingArtifact(xOutputToObs, "X");
postMortemTXGraphicObservingAgent.addInputCouplingArtifact(xOutputToObs, "X");
CentralizedEventCouplingArtifact yOutputToObs = new CentralizedEventCouplingArtifact();
agentY.addOutputCouplingArtifact(yOutputToObs, "Y");
postMortemTXGraphicObservingAgent.addInputCouplingArtifact(yOutputToObs, "Y");
CentralizedEventCouplingArtifact zOutputToObs = new CentralizedEventCouplingArtifact();
agentZ.addOutputCouplingArtifact(zOutputToObs, "Z");
postMortemTXGraphicObservingAgent.addInputCouplingArtifact(zOutputToObs, "Z");
```
		
![PostMortemTXGraphic](images/PostMortemTXGraphic.png)
			
### LiveBarGraphic and PostMortemBarGraphic

BarGraphic requires 5 arguments:

- the title of the graph
- the name of the X and Y axis
- the list of the names of the bars (be sure that the names of the bars are in the same order than the vector output by the port)
- the name of the port to observe

Note:

- BarGraphic only manages a SimulVector of real.
- The slide bar is used to indicate which time of the simulation you want visualize.

```java
	} else if (port.equals("vector3")) {
		SimulEvent result = new SimulEvent(new ArrayedSimulVector<>(equation.getVariable("X"), equation.getVariable("Y"), equation.getVariable("Z")), equation.getTime());
		return result;	
	}
```

---

```java
ObservingMAgent postMortemBarGraphicObservingAgent = new ObservingMAgent("Lorenz observer", maxSimulationTime);
PostMortemBarGraphic liveBarGraphic = new PostMortemBarGraphic("Lorenz", "bars", "values", 
		new String[]{"X","Y","Z"}, "XYZ");
postMortemBarGraphicObservingAgent.setDispatcherArtifact(liveBarGraphic);
CentralizedEventCouplingArtifact zOutputToObs = new CentralizedEventCouplingArtifact();
agentZ.addOutputCouplingArtifact(zOutputToObs, "vector3");
postMortemBarGraphicObservingAgent.addInputCouplingArtifact(zOutputToObs, "XYZ");
```

![PostMortemBarGraphic](images/PostMortemBarGraphic.png)


### LivePieGraphic and PostMortemPieGraphic

PieGraphic requires 3 arguments:

- the title of the graph
- the list of the names of the bars (be sure that the names of the bars are in the same order than the vector output by the port)
- the name of the port to observe

Note:

- PieGraphic only manages a SimulVector of real (same as BarGraphic).
- The slide bar is used to indicate which time of the simulation you want visualize.

```java
ObservingMAgent livePieGraphicObservingAgent = new ObservingMAgent("Lorenz observer", maxSimulationTime);
LivePieGraphic livePieGraphic = new LivePieGraphic("Lorenz", 
		new String[]{"X","Y","Z"}, "XYZ");
livePieGraphicObservingAgent.setDispatcherArtifact(livePieGraphic);
CentralizedEventCouplingArtifact zOutputToObs = new CentralizedEventCouplingArtifact();
agentZ.addOutputCouplingArtifact(zOutputToObs, "vector3");
livePieGraphicObservingAgent.addInputCouplingArtifact(zOutputToObs, "XYZ");
```

![LivePieGraphic](images/LivePieGraphic.png)


### PostMortemEventGraphic and LiveEventGraphic

To plot a temporal visualization of a variable, you need to provide:

- the title of the graph
- the names of the x ("time") and y axis
- the name of the series
- the name of the port to observe

Note:

- EvenGraphic only manage real.
- The graphic shows a peak for each value change of the port observed. The height of the peak corresponds to the new value of the port.

```java
ObservingMAgent postMortemEventGraphicObservingAgent = new ObservingMAgent("Lorenz observer", maxSimulationTime);
PostMortemEventGraphic postMortemEventGraphic = new PostMortemEventGraphic("Lorenz", "time", "X", "X", "X");		postMortemEventGraphicObservingAgent.setDispatcherArtifact(postMortemEventGraphic);
CentralizedEventCouplingArtifact xOutputToObs = new CentralizedEventCouplingArtifact();
agentX.addOutputCouplingArtifact(xOutputToObs, "X");
postMortemEventGraphicObservingAgent.addInputCouplingArtifact(xOutputToObs, "X");
```

![PostMortemEventGraphic](images/PostMortemEventGraphic.png)


### PostMortem3DGraphic and Live3DGraphic

3DGraphic requires 5 arguments:

- title of the graph
- names of the x, y, and z axis
- name of the port to observe. 3DGraphic only manage Tuple3

```java
else if (port.equals("obs3d")) {
SimulEvent result = new SimulEvent(new Tuple3<Double, Double, Double>(equation.getVariable("X"), equation.getVariable("Y"), equation.getVariable("Z")), equation.getTime());
return result;
```

```java
ObservingMAgent postMortem3DGraphicObservingAgent = new ObservingMAgent("Lorenz observer", maxSimulationTime);
PostMortem3DGraphic postMortem3DGraphic = new PostMortem3DGraphic("Lorenz", "X", "Y", "Z", "XYZ");
postMortem3DGraphicObservingAgent.setDispatcherArtifact(postMortem3DGraphic);
CentralizedEventCouplingArtifact zOutputToObs = new CentralizedEventCouplingArtifact();
agentZ.addOutputCouplingArtifact(zOutputToObs, "obs3d");
postMortem3DGraphicObservingAgent.addInputCouplingArtifact(zOutputToObs, "XYZ");
```

This example doesn't work yet with the container.

## Multiple graphs / dispatcher

Usually we cannot link more than one artifact to the same agent. To connect multiple observing artifacts to the same agent, we use a dispatcher; the dispatcher is the single artifact attached to the agent, and act as a proxy for as many observing artifacts as you attach to it.

```java
// Dispatcher
ObservingMAgent multipleGraphicObservingAgent = new ObservingMAgent("Lorenz observer", maxSimulationTime);
SwingDispatcherArtifact swingDispatcherArtifact = new SwingDispatcherArtifact();
multipleGraphicObservingAgent.setDispatcherArtifact(swingDispatcherArtifact);

// Attach observers to the dispatcher
// PostMortemXYGraphic
PostMortemXYGraphic postMortemXYGraphic = new PostMortemXYGraphic("Lorenz XY", "X", "Y", Renderer.Line, "XY", "XY");
swingDispatcherArtifact.addObservingArtifact(postMortemXYGraphic);
CentralizedEventCouplingArtifact xOutputToObs2d = new CentralizedEventCouplingArtifact();
agentX.addOutputCouplingArtifact(xOutputToObs2d, "obs2d");
multipleGraphicObservingAgent.addInputCouplingArtifact(xOutputToObs2d, "XY");
// PostMortemTXGraphic
PostMortemTXGraphic postMortemTXGraphic = new PostMortemTXGraphic("Lorenz TX", "value", Renderer.Line, 
		new String[] {"X series", "Y series", "Z series"}, 
		new String[] {"X", "Y", "Z"});
swingDispatcherArtifact.addObservingArtifact(postMortemTXGraphic);
CentralizedEventCouplingArtifact xOutputToObs = new CentralizedEventCouplingArtifact();
agentX.addOutputCouplingArtifact(xOutputToObs, "X");
multipleGraphicObservingAgent.addInputCouplingArtifact(xOutputToObs, "X");
CentralizedEventCouplingArtifact yOutputToObs = new CentralizedEventCouplingArtifact();
agentY.addOutputCouplingArtifact(yOutputToObs, "Y");
multipleGraphicObservingAgent.addInputCouplingArtifact(yOutputToObs, "Y");
CentralizedEventCouplingArtifact zOutputToObs = new CentralizedEventCouplingArtifact();
agentZ.addOutputCouplingArtifact(zOutputToObs, "Z");
multipleGraphicObservingAgent.addInputCouplingArtifact(zOutputToObs, "Z");
// PostMortemPieGraphic
PostMortemPieGraphic postMortemPieGraphic = new PostMortemPieGraphic("Lorenz Pie", 
		new String[]{"X","Y","Z"}, "XYZ");
swingDispatcherArtifact.addObservingArtifact(postMortemPieGraphic);
CentralizedEventCouplingArtifact zOutputToObsV3 = new CentralizedEventCouplingArtifact();
agentZ.addOutputCouplingArtifact(zOutputToObsV3, "vector3");
multipleGraphicObservingAgent.addInputCouplingArtifact(zOutputToObsV3, "XYZ");
```

If the observers have the same type (for instance "PostMortem" above), the graphs are displayed on the same window (see below). To display graphs in separate windows, create separate observing agents.

![SwingDispatcher](images/SwingDispatcher.png)


