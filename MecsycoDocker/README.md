# Dockerized version of Mecsyco-java

How to:
1. run the container: `docker run --rm --detach --publish 6080:80 registry.gitlab.inria.fr/vgaltier/act_4_ie/mecsyco:latest`
2. use a web browser and visit [http://127.0.0.1:6080/](http://127.0.0.1:6080/)
3. start Eclipse (`eclipse`), and open the `/root/quickstart` project, its pom.xml file contains all the required dependencies

Notes: 
- sudo password is "ubuntu"
- if you want to "mount"/share a host directory to the `/workspace` directory in the container: `docker run --rm --detach --publish 6080:80 --volume YOUR_CHOICE_OF_PATH:/workspace:rw registry.gitlab.inria.fr/vgaltier/act_4_ie/mecsyco:latest`
- to use your host user name and id (XXXX is given by `id -u` and YYYY by `id -n -u`): `docker run --rm --detach --publish 6080:80 --volume YOUR_CHOICE_OF_PATH:/workspace:rw --env USERNAME=YYYYYY --env USERID=XXXX registry.gitlab.inria.fr/vgaltier/act_4_ie/mecsyco:latest`
