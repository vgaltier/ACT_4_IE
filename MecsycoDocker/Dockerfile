# Usage:

# Build:
# docker build -t mecsyco .

# Run:
# docker run --rm --detach --publish 6080:80 mecsyco:latest
# or, if you want to "mount" a host directory to the /workspace directory in the container:
# docker run --rm --detach --publish 6080:80 --volume YOUR_CHOICE_OF_PATH:/workspace:rw mecsyco:latest
# and to use your host user name and id (XXXX is given by id -u and YYYY by id -n -u):
# docker run --rm --detach --publish 6080:80 --volume YOUR_CHOICE_OF_PATH:/workspace:rw --env USERNAME=YYYYYY --env USERID=XXXX mecsyco:latest

# Connect to:
# Use a web browser and visit http://127.0.0.1:6080/

# note: sudo password is "ubuntu"

FROM fredblgr/ubuntu-novnc:20.04   

RUN	apt-get update

# Set time zone to avoid interaction during install
ENV	TZ=Europe/Paris
RUN	ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN	apt-get install -y maven unzip git


# Install Mecsyco
#----------------
# Get Mecsyco
RUN	git clone https://gitlab.inria.fr/Simbiot/mecsyco/mecsycojava.git \
&&	mv mecsycojava /opt/
# Install NetLogo
RUN	wget https://ccl.northwestern.edu/netlogo/6.2.2/NetLogo-6.2.2-64.tgz \
&&	tar -xvf NetLogo-6.2.2-64.tgz \
&&	mv NetLogo\ 6.2.2 /opt/NetLogo-6.2.2 \
&&	rm NetLogo-6.2.2-64.tgz
# adapt Mecsyco pom.xml to NetLogo install
RUN	mv /opt/mecsycojava/mecsyco/mecsyco-world-netlogo/pom.xml /opt/mecsycojava/mecsyco/mecsyco-world-netlogo/pom.xml.old \
&&	head -n 31 /opt/mecsycojava/mecsyco/mecsyco-world-netlogo/pom.xml.old > /opt/mecsycojava/mecsyco/mecsyco-world-netlogo/pom.xml \
&&	echo "                        <version>6.2.2</version>" >> /opt/mecsycojava/mecsyco/mecsyco-world-netlogo/pom.xml \
&&	echo "                        <scope>system</scope>" >> /opt/mecsycojava/mecsyco/mecsyco-world-netlogo/pom.xml \
&&	echo "                        <systemPath>/opt/NetLogo-6.2.2/app/netlogo-6.2.2.jar</systemPath>" >> /opt/mecsycojava/mecsyco/mecsyco-world-netlogo/pom.xml \
&&	tail -n 3 /opt/mecsycojava/mecsyco/mecsyco-world-netlogo/pom.xml.old >> /opt/mecsycojava/mecsyco/mecsyco-world-netlogo/pom.xml
# Install DDS
RUN	wget http://mecsyco.com/dev/download/dds/opensplice-linux64-cpp_6.4-mecsyco.tgz -P /tmp/ \
&&	mkdir -p /opt/mecsyco-cpp/lib/dds/ \
&&	tar xf /tmp/opensplice-linux64-cpp_*.tgz -C /opt/mecsyco-cpp/lib/dds/
ENV	OSPL_HOME=/opt/mecsyco-cpp/lib/dds/HDE/x86_64.linux
ENV	OSPL_PATH=$OSPL_HOME/bin;$OSPL_HOME/lib
ENV	OSPL_URI=file://$OSPL_HOME/ospl.xml
ENV	PATH=$OSPL_PATH:$PATH
RUN	mvn install:install-file -Dfile=$OSPL_HOME/jar/dcpssaj.jar -DgroupId=org.opensplice -DartifactId=dcpssaj-linux-x64 -Dversion=6.4 -Dpackaging=jar \
&&	ln -s $OSPL_HOME/lib/lib*.so /usr/lib/ 
# adapt Mecsyco pom.xml to DDS install on Linux
RUN	sed -i 's/dcpssaj-windows-x64/dcpssaj-linux-x64/' /opt/mecsycojava/mecsyco/mecsyco-communication-dds/pom.xml
# Install Mecsyco
RUN	mvn -f /opt/mecsycojava/mecsyco/pom.xml install -DskipTests -fae


# Install Eclipse
#-----------------
RUN	wget https://mirror.ibcp.fr/pub/eclipse/technology/epp/downloads/release/2021-12/R/eclipse-java-2021-12-R-linux-gtk-x86_64.tar.gz \
&&	zcat eclipse-java-2021-12-R-linux-gtk-x86_64.tar.gz | tar xvf - \
&&	mv eclipse /opt \
&&	rm eclipse-java-2021-12-R-linux-gtk-x86_64.tar.gz	
ENV	PATH=$PATH:/opt/eclipse
# add Eclipse item to start menu:
COPY	eclipse.desktop /usr/share/applications/eclipse.desktop
RUN	chmod a+r /usr/share/applications/eclipse.desktop


# Create a maven project with the Mecsyco dependencies
COPY	quickstart quickstart

RUN	echo "export PATH=$PATH" > /etc/environment
